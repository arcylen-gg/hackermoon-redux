import ACTIONS from "./action";
import _ from "lodash";

const defaultState = {
    items: []
};

const blogReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTIONS.Types.CREATE_BLOG: {
            console.log(action);

            let item = action.payload;
            let newBlog = { 
                id: state.items.length + 1,
                description: item
            };
            let newState = _.cloneDeep(state);
            newState.items.push(newBlog);
            return newState;
        }

        case ACTIONS.Types.DELETE_BLOG: {
            let newState = _.cloneDeep(state);
            let index = _.findIndex(newState.items, { id: action.payload });
            newState.items.splice(index, 1);
            return newState;
        }

        default: 
            return state;
    }
}

export default blogReducer;