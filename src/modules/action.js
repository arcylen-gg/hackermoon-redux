// types of action
const Types = {
    CREATE_BLOG: "CREATE_BLOG",
    DELETE_BLOG: "DELETE_BLOG"
};
// actions
const createBlog = task => ({
    type: Types.CREATE_BLOG,
    payload: task
});

const deleteBlog = id => ({
    type: Types.DELETE_BLOG,
    payload: id
});

export default {
    createBlog,
    deleteBlog,
    Types
};